export class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  speak() {}

  walk() {
    console.log(`${this.name} can walk`);
  }

  read() {}

  write() {}
}

export var PI = 3.14;

var age = 19;

// below 16-> not
// 16 te 18 -> non gear
// 18 -> gear

// below 16,138-> not
// 16,239 te 18,454 -> non gear
// 19,345 -> gear

if (age < 16) {
  console.log("not eligile");
} else {
  if (age >= 16 && age < 18) {
    console.log("non gear");
  } else {
    console.log("gear");
  }
}

if (age >= 16 && age < 18) {
  console.log("non gear");
} else if (age < 16) {
  console.log("not eligile");
} else {
  console.log("gear");
}

if (age > 18) {
  console.log("gear");
} else if (age >= 16) {
  console.log(" non gear");
} else {
  console.log("not eligile");
}

// if (age >= 16) {
//   console.log("Eligible for driving non-gear licence");
// } else {
//   console.log("not eligible");
// }

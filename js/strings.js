var name = "Karan";
var surname = "Sharma";

var fullName = name + " " + surname;
var fullName1 = name.concat(" ", surname);
var fullName2 = `My name is ${name} ${surname}`; //es6

console.log(fullName);
console.log(fullName1);
console.log(fullName2);
console.log(fullName.toUpperCase());
console.log(fullName.indexOf("S"));

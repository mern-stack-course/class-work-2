var student = {
  name: "karan sharma",
  rollNumber: 20,
  standard: 4,
  age: 43,
  sayPoem: function () {
    console.log("Jonny jonny yes papa");
  },
};

console.log("Student:", student);
console.log("Student name:", student.name);
console.log("Student rollnumber:", student.rollNumber);
console.log("Student standard:", student["standard"]);
console.log("Student sayPoem:", student["sayPoem"]);
student.sayPoem();

console.log("------------For in array-------");
var numbers = [12, 34, 15, 18];
for (var index in numbers) {
  console.log(`index: ${index}, value: ${numbers[index]}`);
}

console.log("------------For of array-------");
for (var value of numbers) {
  console.log(`Value:${value}`);
}

console.log("------------For in object-------");
for (var key in student) {
  console.log(`Key: ${key}, value: ${student[key]}`);
}

// console.log("------------For of object-------");
// for (var value of student) {
//   console.log(` value: ${value}`);
// }

console.log("value array", Object.values(student));
console.log("key array", Object.keys(student));
for (var value of Object.values(student)) {
  console.log(` value: ${value}`);
}

console.log("--------counter using object-----------");
var counterObject = {
  count: 0,
  getCount: function () {
    this.count++;
    return this.count;
  },
};

console.log(counterObject.getCount());
console.log(counterObject.getCount());
console.log(counterObject.getCount());

add = adder(4);
add(5); //9
add(10); //19
add(20); //39

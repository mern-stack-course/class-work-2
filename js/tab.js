const content = ["Tab 1 content", "Tab 2 content", "Tab 3 content"];

const btnTab1 = document.getElementById("btn-tab-1");
const btnTab2 = document.getElementById("btn-tab-2");
const btnTab3 = document.getElementById("btn-tab-3");
const tabContent = document.getElementById("tab-content");

function displayContent(content) {
  tabContent.innerHTML = content;
}

function highlightButton(btn) {
  btnTab1.className = "";
  btnTab2.className = "";
  btnTab3.className = "";
  btn.className = "active";
}

function handleClick(event) {
  const btnId = event.target.id;
  highlightButton(event.target);
  if (btnId === "btn-tab-1") {
    displayContent(content[0]);
  } else if (btnId === "btn-tab-2") {
    displayContent(content[1]);
  } else {
    displayContent(content[2]);
  }
}

displayContent(content[0]); // initially show this content

btnTab1.addEventListener("click", handleClick);
btnTab2.addEventListener("click", handleClick);
btnTab3.addEventListener("click", handleClick);

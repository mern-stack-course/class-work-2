// console.log("this:", this);

// function sayHello() {
//   console.log("inside hello func", this);
// }

// sayHello();

// var counterObject = {
//   count: 0,
//   getCount: function () {
//     this.count++;
//     return this.count;
//   },
// };

// console.log(counterObject.getCount());

// var number = 10;
// function printNumber() {
//   console.log("Number:", this.karan);
// }

// printNumber();

// var student1 = {
//   name: "Karan",
//   age: 16,
// };

// var student2 = {
//   name: "Sham",
//   age: 15,
// };

// var teacher1 = {
//   name: "Rocky",
// };

// function sayHello() {
//   console.log("Good Morning, ", this.name);
// }

// function printStudent(token) {
//   console.log(
//     `print token: ${token} Student Name: ${this.name}, age: ${this.age}`
//   );
// }

// printStudent.apply(student2, [1]);

// var printstudent1 = printStudent.bind(student1);

// printstudent1(2);

// sayHello.call(student1);
// sayHello.call(teacher1);

//-------Object Creation------

//object literal
var student1 = {
  name: "Karan",
  age: 20,
  sayHello: function () {
    console.log(`Good Morning ${this.name}`);
  },
};

student1.sayHello();
//create method
var student2 = Object.create({});
student2.name = "Sham";
student2.age = 23;
student2.sayHello = function () {
  console.log(`Good Morning ${this.name}`);
};
student2.sayHello();
//constructer

function Student(name, age) {
  this.name = name;
  this.age = age;
  this.sayHello = function () {
    console.log(`Good Morning ${this.name}`);
  };
}

var student3 = new Student("Ram", 21);
student3.sayHello();
var student4 = new Student("Ashish", 22);
var student5 = new Student("rohit", 12);

//class

class MyStudent {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}

var student10 = new MyStudent("om", 20);

var myA = {
  href: "abc.com",
  target: "_blank",
  displaName: "display name",
};

//<a href="abc.com" target="_blank">diplay name</a>

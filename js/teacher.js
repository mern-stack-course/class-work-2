import { Person } from "./utility.js";
class Teacher extends Person {
  constructor(name, age, dept, std) {
    super(name, age);
    this.dept = dept;
    this.std = std;
  }

  giveLecture() {}
}

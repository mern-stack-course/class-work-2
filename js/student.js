import { Person, PI } from "./utility.js";
class SuperStudent extends Person {
  constructor(name, age, rollNuber, std) {
    super(name, age);
    this.rollNuber = rollNuber;
    this.std = std;
  }

  learn() {
    console.log(`${this.name} can learn ${PI}`);
  }
  attendClass() {}
}

var student10 = new SuperStudent("Roshan", 22, 2, "5th");
student10.walk();
student10.learn();

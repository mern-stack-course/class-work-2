// function greet() {
//   console.log("Good Morning");
// }

// var greet = function () {
//   console.log("Good Morning");
// };

// var greet = () => {
//   console.log("Good Morning");
//   console.log("asd");
// };
// greet();

// var student = { name: "Karan", age: 20 };

// //var age = student.age;

// var { age, name } = student; //destructing

// console.log(name, age);

// var students = ["karan", "sham", "mahesh"];

// console.log(students[0]);
// const [firstStudent, anotherStudent] = students;
// console.log(firstStudent, anotherStudent);

// var student1 = student[0];

//-------------
var student1 = {
  name: "Karan",
  age: 20,
  speak: function () {
    console.log(`${this.name} can speak in englis`);
  },
};

var student2 = {
  name: "Sham",
  age: 20,
  speak: function () {
    console.log(`${this.name} can speak in english`);
  },
};

student1.speak();
student2.speak();

function Student(name, age) {
  this.name = name;
  this.age = age;

  this.speak = function () {
    console.log(`${this.name} can speak in english`);
  };
}

var student3 = new Student("Ram", 21);
student3.speak();

var student4 = new Student("Rohit", 21);
student4.speak();

class MyStudent {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  speak() {
    console.log(`${this.name} can speak in english`);
  }
}

var student5 = new MyStudent("Raj", 24);
student5.speak();

var student6 = new MyStudent("Akash", 24);
student5.speak();

class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  speak() {}

  walk() {
    console.log(`${this.name} can walk`);
  }

  read() {}

  write() {}
}

class SuperStudent extends Person {
  constructor(name, age, rollNuber, std) {
    super(name, age);
    this.rollNuber = rollNuber;
    this.std = std;
  }

  learn() {
    console.log(`${this.name} can learn`);
  }
  attendClass() {}
}

class Teacher extends Person {
  constructor(name, age, dept, std) {
    super(name, age);
    this.dept = dept;
    this.std = std;
  }

  giveLecture() {}
}

var student10 = new SuperStudent("Roshan", 22, 2, "5th");
student10.walk();
student10.learn();

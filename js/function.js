//var counter = 0; //global varaible

function getCount() {
  var counter = 0; //functional scope
  //counter = counter + 1;
  ++counter;
  return counter;
  //counter = counter + 1
}

console.log(getCount()); //1
//console.log("counter:", counter); // error
console.log(getCount()); //2
console.log(getCount()); //3

function sayHello() {
  console.log("hello");
  counter = 1; // global
}
sayHello();
console.log(getCount()); //4
console.log(getCount()); // 5
console.log("counter:", counter);

//closure
// var a = 25; //global
// function outerFunction() {
//   var a = 10; //local, lexical scope
//   function innnerFunction() {
//     var b = 0;
//     console.log(a);
//   }
//   return innnerFunction;
// }

// var innerFunc = outerFunction()

// innerFunc()

console.log("-----------------Clouser-----------");
function getCounter() {
  var count = 0;
  function counterIncrement() {
    count++;
    return count;
  }
  return counterIncrement;
}

var getCount = getCounter();
console.log("count", getCount());
console.log("count", getCount());
console.log("count", getCount());
console.log("count:", count); //error

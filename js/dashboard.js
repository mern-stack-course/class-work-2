var isUserLoggedIn = sessionStorage.getItem("isUserLoggedIn");

if (isUserLoggedIn) {
  console.log("user logged in");
} else {
  console.log("user not logged in");
  location.href = "./login.html";
}

// var linkObject = document.getElementById("btnLogout");
// linkObject.addEventListener("click", logout);

function logout(event) {
  event.preventDefault();
  sessionStorage.removeItem("isUserLoggedIn");
  location.href = "../index.html";
}

function showAlert() {
  alert("I am alert");
}

//showAlert();
// setTimeout(showAlert, 3000); //ms

// console.log("first log");
// function greet(userName) {
//   alert(`Good Morning, ${userName}`);
//   console.log("second log");
// }

// console.log("third log");
// var newGreet = greet.bind(this, "Ram");

// setTimeout(newGreet, 3000);

// console.log("fourth log");

//------------

// function myFirstFunc() {
//   console.log("first func");
//   mySecondFunc();
//   console.log("another line in first func");
// }

// function mySecondFunc() {
//   console.log("second func");
// }

// myFirstFunc();

function printer(num) {
  setTimeout(function () {
    console.log(num);
  }, 2000);
}

for (var i = 0; i < 5; i++) {
  // (function (i) {
  //   setTimeout(function () {
  //     console.log(i);
  //   }, 2000);
  // })(i);
  printer(i);
}

//1,2,3,4,5
//0,1,2,3,4 -
//5,5,5,5,5

//arrow function
//array and object destrucring
//class
